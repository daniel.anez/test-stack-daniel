exports.handler = async (event, context) => {
  // Log the event argument for debugging and for use in local development.
  console.log(JSON.stringify(event, undefined, 2));
  let sampleTest = process.env.SAMPLE_TEST;
  let sampleText = process.env.SAMPLE_TEXT;

  const response = {
    statusCode: 200,
    body: JSON.stringify({
      msg1: sampleText,
      msg2: sampleTest
    }),
};

  return response;
};
